import { gql } from 'apollo-server';
import { merge } from 'lodash';
import hotel from './hotel';
import user from './user';
import healthcheck from './healthcheck';

export const typeDefs = [
    gql`type Query
`,
    hotel.typeDefs,
    user.typeDefs,
    healthcheck.typeDefs,
];

export const resolvers = merge({}, 
    hotel.resolvers,
    user.resolvers,
    healthcheck.resolvers,
);
