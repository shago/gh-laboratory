import { gql } from 'apollo-server';

const typeDefs = gql`
  type LoginResponse {
    "Id of the authenticated user"
    id: ID!
    username: String!
    "JWT Token"
    token: String!
    "Role of the User"
    role: String!
  }

  input LoginInput {
      username: String!
      password: String!
  }

  extend type Query {
    "Authenticate user."
    login(params: LoginInput!): LoginResponse!
  }
`;

export default typeDefs;
