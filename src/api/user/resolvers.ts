import { users } from '../../data/mongo';
import { LoginInput, LoginResponse, QueryLoginArgs } from '../types/graphql-types';


export default {
    Query: {
        login: async (
            _: unknown,
            { params: { username, password } }: QueryLoginArgs,
            context: unknown
        ):
        Promise<LoginResponse> => {
            console.log('Login!', );
            const { user, token } = await users.login(username, password);
            console.log(user, token);
            return { username: user.username, id: user.id, role: user.role, token };
        }
    }
};
