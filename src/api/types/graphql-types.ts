import { GraphQLResolveInfo } from 'graphql';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export enum Amenities {
  /** air conditioning */
  AirConditioning = 'AIR_CONDITIONING',
  /** bathtub */
  Bathtub = 'BATHTUB',
  /** free parking */
  FreeParking = 'FREE_PARKING',
  /** hair dryer */
  HairDryer = 'HAIR_DRYER',
  /** heating */
  Heating = 'HEATING',
  /** iron */
  Iron = 'IRON',
  /** kitchen */
  Kitchen = 'KITCHEN',
  /** parking */
  Parking = 'PARKING',
  /** wifi */
  Wifi = 'WIFI'
}

export type HealthStatus = {
  __typename?: 'HealthStatus';
  db: Scalars['Boolean'];
  external_api: Scalars['Boolean'];
  local_api: Scalars['Boolean'];
};

export type Hotel = {
  __typename?: 'Hotel';
  id: Scalars['ID'];
  name: Scalars['String'];
  rooms: Array<Room>;
  state: Scalars['String'];
};

export type LoginInput = {
  password: Scalars['String'];
  username: Scalars['String'];
};

export type LoginResponse = {
  __typename?: 'LoginResponse';
  /** Id of the authenticated user */
  id: Scalars['ID'];
  /** Role of the User */
  role: Scalars['String'];
  /** JWT Token */
  token: Scalars['String'];
  username: Scalars['String'];
};

export type MetricPrice = {
  __typename?: 'MetricPrice';
  competitor_name: Scalars['String'];
  gross_amount: Scalars['Float'];
  net_amount: Scalars['Float'];
};

export type Metrics = {
  __typename?: 'Metrics';
  average_Price: MetricPrice;
  best_price: MetricPrice;
  worst_price: MetricPrice;
};

export enum Period {
  /** 30 days search */
  Days_30 = 'DAYS_30',
  /** 60 days search */
  Days_60 = 'DAYS_60',
  /** 90 days search */
  Days_90 = 'DAYS_90'
}

export type Price = {
  __typename?: 'Price';
  amount: Scalars['Int'];
  competitor_name: Scalars['String'];
  currency: Scalars['String'];
  date: Scalars['String'];
  taxes: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  getHotelInsights: Array<Room>;
  getHotelMetrics: Array<Room>;
  /** Get the hotel information by its id. */
  hotel: Hotel;
  /** Authenticate user. */
  login: LoginResponse;
  /** Get the health of the service. */
  ping: HealthStatus;
};


export type QueryGetHotelInsightsArgs = {
  hotel_id: Scalars['ID'];
  limit?: InputMaybe<Scalars['Int']>;
  period: Period;
  room_type?: InputMaybe<RoomType>;
};


export type QueryGetHotelMetricsArgs = {
  day: Scalars['String'];
  hotel_id: Scalars['ID'];
  room_type: RoomType;
};


export type QueryHotelArgs = {
  hotelId: Scalars['Int'];
};


export type QueryLoginArgs = {
  params: LoginInput;
};

export type Room = {
  __typename?: 'Room';
  amenities: Array<Amenities>;
  bedCount: Scalars['Int'];
  metrics: Metrics;
  prices: Array<Price>;
  room_id: Scalars['ID'];
  room_name: Scalars['String'];
  room_type: RoomType;
};

export enum RoomType {
  /** Business */
  Business = 'BUSINESS',
  /** Residential */
  Residential = 'RESIDENTIAL'
}

export type WithIndex<TObject> = TObject & Record<string, any>;
export type ResolversObject<TObject> = WithIndex<TObject>;

export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = ResolversObject<{
  Amenities: Amenities;
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>;
  Float: ResolverTypeWrapper<Scalars['Float']>;
  HealthStatus: ResolverTypeWrapper<HealthStatus>;
  Hotel: ResolverTypeWrapper<Hotel>;
  ID: ResolverTypeWrapper<Scalars['ID']>;
  Int: ResolverTypeWrapper<Scalars['Int']>;
  LoginInput: LoginInput;
  LoginResponse: ResolverTypeWrapper<LoginResponse>;
  MetricPrice: ResolverTypeWrapper<MetricPrice>;
  Metrics: ResolverTypeWrapper<Metrics>;
  Period: Period;
  Price: ResolverTypeWrapper<Price>;
  Query: ResolverTypeWrapper<{}>;
  Room: ResolverTypeWrapper<Room>;
  RoomType: RoomType;
  String: ResolverTypeWrapper<Scalars['String']>;
}>;

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = ResolversObject<{
  Boolean: Scalars['Boolean'];
  Float: Scalars['Float'];
  HealthStatus: HealthStatus;
  Hotel: Hotel;
  ID: Scalars['ID'];
  Int: Scalars['Int'];
  LoginInput: LoginInput;
  LoginResponse: LoginResponse;
  MetricPrice: MetricPrice;
  Metrics: Metrics;
  Price: Price;
  Query: {};
  Room: Room;
  String: Scalars['String'];
}>;

export type HealthStatusResolvers<ContextType = any, ParentType extends ResolversParentTypes['HealthStatus'] = ResolversParentTypes['HealthStatus']> = ResolversObject<{
  db?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  external_api?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  local_api?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type HotelResolvers<ContextType = any, ParentType extends ResolversParentTypes['Hotel'] = ResolversParentTypes['Hotel']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  rooms?: Resolver<Array<ResolversTypes['Room']>, ParentType, ContextType>;
  state?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type LoginResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['LoginResponse'] = ResolversParentTypes['LoginResponse']> = ResolversObject<{
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  role?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  token?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  username?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MetricPriceResolvers<ContextType = any, ParentType extends ResolversParentTypes['MetricPrice'] = ResolversParentTypes['MetricPrice']> = ResolversObject<{
  competitor_name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  gross_amount?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  net_amount?: Resolver<ResolversTypes['Float'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type MetricsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Metrics'] = ResolversParentTypes['Metrics']> = ResolversObject<{
  average_Price?: Resolver<ResolversTypes['MetricPrice'], ParentType, ContextType>;
  best_price?: Resolver<ResolversTypes['MetricPrice'], ParentType, ContextType>;
  worst_price?: Resolver<ResolversTypes['MetricPrice'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type PriceResolvers<ContextType = any, ParentType extends ResolversParentTypes['Price'] = ResolversParentTypes['Price']> = ResolversObject<{
  amount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  competitor_name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  currency?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  date?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  taxes?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = ResolversObject<{
  getHotelInsights?: Resolver<Array<ResolversTypes['Room']>, ParentType, ContextType, RequireFields<QueryGetHotelInsightsArgs, 'hotel_id' | 'period'>>;
  getHotelMetrics?: Resolver<Array<ResolversTypes['Room']>, ParentType, ContextType, RequireFields<QueryGetHotelMetricsArgs, 'day' | 'hotel_id' | 'room_type'>>;
  hotel?: Resolver<ResolversTypes['Hotel'], ParentType, ContextType, RequireFields<QueryHotelArgs, 'hotelId'>>;
  login?: Resolver<ResolversTypes['LoginResponse'], ParentType, ContextType, RequireFields<QueryLoginArgs, 'params'>>;
  ping?: Resolver<ResolversTypes['HealthStatus'], ParentType, ContextType>;
}>;

export type RoomResolvers<ContextType = any, ParentType extends ResolversParentTypes['Room'] = ResolversParentTypes['Room']> = ResolversObject<{
  amenities?: Resolver<Array<ResolversTypes['Amenities']>, ParentType, ContextType>;
  bedCount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  metrics?: Resolver<ResolversTypes['Metrics'], ParentType, ContextType>;
  prices?: Resolver<Array<ResolversTypes['Price']>, ParentType, ContextType>;
  room_id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  room_name?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  room_type?: Resolver<ResolversTypes['RoomType'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
}>;

export type Resolvers<ContextType = any> = ResolversObject<{
  HealthStatus?: HealthStatusResolvers<ContextType>;
  Hotel?: HotelResolvers<ContextType>;
  LoginResponse?: LoginResponseResolvers<ContextType>;
  MetricPrice?: MetricPriceResolvers<ContextType>;
  Metrics?: MetricsResolvers<ContextType>;
  Price?: PriceResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Room?: RoomResolvers<ContextType>;
}>;

