import { gql } from 'apollo-server';

const typeDefs = gql`
  enum RoomType {
    "Residential"
    RESIDENTIAL
    "Business"
    BUSINESS
  }

  enum Amenities {
    "wifi"
    WIFI
    "kitchen"
    KITCHEN
    "parking"
    PARKING
    "air conditioning"
    AIR_CONDITIONING
    "heating"
    HEATING
    "iron"
    IRON
    "hair dryer"
    HAIR_DRYER
    "bathtub"
    BATHTUB
    "free parking"
    FREE_PARKING
  }

  enum Period {
    "30 days search"
    DAYS_30
    "60 days search"
    DAYS_60
    "90 days search"
    DAYS_90
  }

  type Price {
    competitor_name: String!
    currency: String!
    taxes: Int!
    amount: Int!
    date: String!
  }

  type MetricPrice {
    competitor_name: String!
    gross_amount: Float!
    net_amount: Float!
  }

  type Metrics {
    best_price: MetricPrice!
    average_Price: MetricPrice!
    worst_price: MetricPrice!
  }

  type Room {
    room_id: ID!
    room_name: String!
    room_type: RoomType!
    bedCount: Int!
    amenities: [Amenities!]!
    prices: [Price!]!
    metrics: Metrics!
  }

  type Hotel {
    id: ID!
    name: String!
    state: String!
    rooms: [Room!]!
  }

  extend type Query {
    "Get the hotel information by its id."
    hotel(
      "Hotel ID you wish to get information for."
      hotelId: Int!
    ): Hotel!
    getHotelInsights(hotel_id: ID!, period: Period!, room_type: RoomType, limit: Int ): [Room!]!
    getHotelMetrics(hotel_id: ID!, day: String!, room_type: RoomType!): [Room!]!
  }
`;

export default typeDefs;
