import { isEmpty } from 'lodash';
import { Hotel, Metrics, Period, Price, QueryGetHotelInsightsArgs, QueryGetHotelMetricsArgs, QueryHotelArgs, Room } from '../types/graphql-types';
import RoomService from '../../service/roomsService';
import HotelService from '../../service/hotelService';

export default {
    Query: {
        hotel: async (
            _: unknown,
            { hotelId }: QueryHotelArgs,
            context: unknown
        ): Promise<Hotel> => {
            if (!context || isEmpty(context)) {
                throw new Error('Permission error');
            }
            return await HotelService.getHotel(hotelId);
        },
        getHotelInsights: async (
            _: unknown,
            { hotel_id, room_type }: QueryGetHotelInsightsArgs,
            context: any
        ): Promise<Array<Room>> => {
            if (!context || context.user?.role !== 'manager') {
                throw new Error('Permission error');
            }
            const hotelId = Number.parseInt(hotel_id);
            return await HotelService.getHotelRooms({ hotelId, roomType: room_type });
        },
        getHotelMetrics: async (
            _: unknown,
            { day, hotel_id, room_type }: QueryGetHotelMetricsArgs,
            context: any
        ): Promise<Array<Room>> => {
            if (!context || context.user?.role !== 'manager') {
                throw new Error('Permission error');
            }
            const hotelId = Number.parseInt(hotel_id);
            return await HotelService.getHotelRooms({ hotelId, roomType: room_type });
        }
    },
    Room: {
        prices: async (
            parent: Room,
            _: unknown,
            context: any,
            { variableValues }: any
        ): Promise<Array<Price>> => {
            const hotelId = Number.parseInt(variableValues.hotelId);
            return RoomService.getRoomPrices({
                hotelId,
                period: variableValues.period as Period,
                roomId: parent.room_id,
                limit: variableValues.limit
            });
        },
        metrics: async(
            parent: Room,
            _: unknown,
            context: any,
            { variableValues }: any 
        ): Promise<Metrics> => {
            const hotelId = Number.parseInt(variableValues.hotelId);
            return await RoomService.getRoomMetrics({ hotelId, roomId: parent.room_id, day: variableValues.day})
        }
    }
};

