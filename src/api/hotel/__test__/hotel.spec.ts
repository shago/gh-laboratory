import hotelService from '../../../service/hotelService';
import roomService from '../../../service/roomsService';
import { Amenities, Period, Room, RoomType } from '../../types/graphql-types';
import resolver from '../resolvers';

const hotelRoomsMock: Room[] = [
  {
    room_id: '4b40127d-d90f-406a-9395-9122256df5d6',
    room_name: 'available',
    room_type: RoomType.Residential,
    bedCount: 4,
    amenities: [
      Amenities.Bathtub
    ],
    prices: [],
    // @ts-ignore
    metrics: {}
  },
  {
    room_id: '4d8c39bb-9c0a-4da2-90f7-691c18cdfb45',
    room_name: 'impact',
    room_type: RoomType.Business,
    bedCount: 5,
    amenities: [
      Amenities.AirConditioning
    ],
    prices: [],
    // @ts-ignore
    metrics: {}
  }
];

const roomPricesMock = [
  {
    competitor_name: "booking",
    currency: "USD",
    taxes: 3,
    amount: 218,
    date: "13/05/2022"
  },
  {
    competitor_name: "expedia",
    currency: "USD",
    taxes: 0,
    amount: 585,
    date: "13/05/2022"
  },
  {
    competitor_name: "travel_com",
    currency: "USD",
    taxes: 0,
    amount: 313,
    date: "13/05/2022"
  },
  {
    competitor_name: "guruhotel",
    currency: "USD",
    taxes: 7,
    amount: 612,
    date: "13/05/2022"
  }
];

describe('Hotel resolver', () => {

  it('should return an error when no token is found', async () => {
    try {
      await resolver.Query.hotel(null, { hotelId: 1 }, null);
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
      const errorAsError = error as Error;
      expect(errorAsError.message).toBe('Permission error');
    }
  });

  it('should get an hotel when calling the "getHotel"', async () => {
    const hotelMock = {
      id: 1,
      name: 'Test',
      rooms: [],
      state: 'Test'
    };
    const getHotelSpy = jest
      .spyOn(hotelService as any, 'getHotel')
      .mockImplementation(() => Promise.resolve(hotelMock));

    const hotelResult = await resolver.Query.hotel(null, { hotelId: 1 }, 'token');

    expect(getHotelSpy).toBeCalled();
    expect(hotelResult).toBe(hotelMock);
  });

  it('should verify the caller is a manager when calling "getHotelInsights"', async () => {
    try {
      await resolver.Query.getHotelInsights(null, { hotel_id: '1', period: Period.Days_30 }, { user: { role: 'public' } });
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
      const errorAsError = error as Error;
      expect(errorAsError.message).toBe('Permission error');
    }
  });

  it('should verify the caller is a manager when calling "getHotelMetrics"', async () => {
    try {
      await resolver.Query.getHotelMetrics(null, { hotel_id: '1', room_type: RoomType.Business, day: '' }, { user: { role: 'public' } });
    } catch (error) {
      expect(error).toBeInstanceOf(Error);
      const errorAsError = error as Error;
      expect(errorAsError.message).toBe('Permission error');
    }
  });

  it('should make a call to "getHotelRooms" when calling getHotelInsights"', async () => {

    const getHotelRoomsSpy = jest
      .spyOn(hotelService as any, 'getHotelRooms')
      .mockImplementation(() => Promise.resolve(hotelRoomsMock));

    const roomsResult = await resolver.Query.getHotelInsights(null, { hotel_id: '1', period: Period.Days_30 }, { user: { role: 'manager' } });

    expect(getHotelRoomsSpy).toBeCalled();
    expect(roomsResult).toBe(hotelRoomsMock);
  });

  it('should make a call to "getRoomPrices" from RoomsService when calling the prices for the room"', async () => {
    const getRoomPricesSpy = jest
      .spyOn(roomService as any, 'getRoomPrices')
      .mockImplementation(() => Promise.resolve(roomPricesMock));
    const roomParent = hotelRoomsMock[0];

    const roomsResult = await resolver.Room.prices(
      roomParent,
      null,
      { user: { role: 'manager' } },
      {variableValues: { period: Period.Days_30, hotelId: "1", limit: null, }});

    expect(getRoomPricesSpy).toBeCalled();
    expect(roomsResult).toBe(roomPricesMock);
  });
});
