import { isEmpty } from 'lodash';
import { connection } from '../../data/mongo';
import { HotelService } from '../../external/hotel-service/codegen';

export default {
    Query: {
        ping: (
            _: unknown,
            args: unknown,
            context: unknown
        ):
        any => {
            return {
                db: connection.isConnected()
            };
        }
    },
    HealthStatus: {
        db: async (_: unknown,
            args: unknown,
            context: unknown) => {
                return connection.isConnected();
        },
        external_api: async(_: unknown,
            args: unknown,
            context: unknown) => {
                const status = await HotelService.getPing();
                return status.healthy;
            },
        local_api: async(_: unknown,
            args: unknown,
            context: unknown) => {
                if (!context || isEmpty(context)) {
                    throw new Error('Permission error');
                }
                return {
                    local_api: true
                };
            }
    }
};
