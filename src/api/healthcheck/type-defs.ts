import { gql } from 'apollo-server';

const typeDefs = gql`
  type HealthStatus {
    db: Boolean!
    local_api: Boolean!
    external_api: Boolean!
  }

  extend type Query {
    "Get the health of the service."
    ping: HealthStatus!
  }
`;

export default typeDefs;
