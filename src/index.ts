import { ApolloServer } from 'apollo-server';
import { verify } from 'jsonwebtoken';
import { resolvers, typeDefs } from './api';
import { seed } from './data/mongo';
import { OpenAPI } from './external/hotel-service/codegen';

console.log('Running...');

const server = new ApolloServer({
  typeDefs,
  resolvers,
  csrfPrevention: true,
  context: ({ req }) => {
    const isLogin = req.body.query.includes('login');
    if (isLogin) {
      return null;
    }
    const token = req.headers.authorization || '';
    if (!token) {
      return null;
    }
    const verified = verify(token, process.env.TOKEN_SECRET!);
    return { user: verified, token };
  },
});
console.log(process.env.HOTEL_SERVICE_URL);
OpenAPI.BASE = process.env.HOTEL_SERVICE_URL || '';

seed();

server.listen().then(({ url }) => {
  console.log(`Server ready at ${url}`);
});



