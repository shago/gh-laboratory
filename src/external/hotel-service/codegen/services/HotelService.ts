/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class HotelService {

    /**
     * @param hotelId
     * @returns any Success
     * @throws ApiError
     */
    public static getHotels(
        hotelId: number,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/hotels/{hotel_id}',
            path: {
                'hotel_id': hotelId,
            },
        });
    }

    /**
     * @param hotelId
     * @param startDate Start date in dd/mm/yyyy format
     * @param endDate End date in dd/mm/yyyy format
     * @returns any
     * @throws ApiError
     */
    public static getPrices(
        hotelId: number,
        startDate?: string,
        endDate?: string,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/hotels/{hotel_id}/prices',
            path: {
                'hotel_id': hotelId,
            },
            query: {
                'start_date': startDate,
                'end_date': endDate,
            },
            errors: {
                400: `Date range cannot be more than 30 days`,
                401: `Unexpected error`,
                404: `Hotel not found`,
            },
        });
    }

    /**
     * @param hotelId
     * @returns any Success
     * @throws ApiError
     */
    public static getRooms(
        hotelId: number,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/hotels/{hotel_id}/rooms',
            path: {
                'hotel_id': hotelId,
            },
        });
    }

    /**
     * @returns any Success
     * @throws ApiError
     */
    public static getPing(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/ping',
        });
    }

}