import NodeCache from 'node-cache';

const localCache = new NodeCache({ stdTTL: 600 });

export default localCache;