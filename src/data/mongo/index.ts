import connection from './connection';
import users, { User } from './models/users';

export {
    connection,
    users
}

export const seed = async (): Promise<void> => {
    connection.connect();
    const userCount = await User.count();
    if (userCount === 0) {
        console.log('No user found, creating users');
        await Promise.all([
            users.createUser('Admin', 'Pass.word1', 'manager'),
            users.createUser('Normal', 'Pass.word1', 'public')
        ]);
    }
}
