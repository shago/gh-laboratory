import mongoose from 'mongoose';
import connection from '../connection';
import { genSalt, hash } from 'bcrypt';
import { sign } from 'jsonwebtoken';

const userSchema = new mongoose.Schema({ 
    username: {
        type: String,
        required: true,
        unique: true,
        min: 3
    },
    password: {
        type: String,
        required: true,
    },
    salt: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true,
    }
});

export const User = mongoose.model('User', userSchema)

const createUser = async (username: string, password: string, role: 'manager' | 'public'): Promise<typeof User> => {
    console.log(`Creating user with username: ${username}`);
    await connection.connect();
    const salt = await genSalt(10);
    const hashedPass = await hash(password, salt);
    return await User.create({ username, password: hashedPass, salt, role });
};

const login = async(username: string, password: string) => {
    await connection.connect();
    console.log(`Trying to fetch '${username}'`);
    const user = await User.findOne({ username });
    if (!user) {
        console.error('Invalid login attempt: User doesn\'t exist');
        throw new Error('Invalid login attempt.');
    }
    const hashedPass = await hash(password, user.salt);
    if (hashedPass !== user.password) {
        console.error('Invalid login attempt: Wrong password');
        throw new Error('Invalid login attempt.');
    }
    const token = sign({
        sub: user.username,
        role: user.role,
    }, process.env.TOKEN_SECRET!);

    return { user, token };
};

export default {
    login,
    createUser
}