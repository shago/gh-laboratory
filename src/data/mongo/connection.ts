import mongoose from 'mongoose';

const connect = async (): Promise<void> => {
    console.log(`Mongoose connection state: ${mongoose.connection?.readyState}`);
    if (!mongoose.connection || mongoose.connection.readyState !== 1) {
        await mongoose.connect(process.env.MONGO_URL || '', {
            keepAlive: true,
            keepAliveInitialDelay: 300000
        });
    }
}

const isConnected = async (): Promise<boolean> => {
    try {
        return mongoose.connection?.readyState === 1;
    } catch (error) {
        console.log(error);
        return false;
    }
}

export default {
    connect,
    isConnected
}