import { createClient, RedisClientType } from 'redis';

let client: RedisClientType;

const getConnection = async (): Promise<RedisClientType> => {
    if (!client) {
        console.log(`No client, creating a new client to ${process.env.REDIS_URL}.`);
        client = createClient({
            url: process.env.REDIS_URL
        });
        client.on('error', (err) => console.log('Redis Client Error', err));
    }
    if (!client.isOpen)  {
        console.log('Opening a connection.');
        await client.connect()
    }
    return client;
}

export const getStatus = async (): Promise<boolean> => {
    try {
        const con = await getConnection();
        console.log(await con.ping());
        return true;
    } catch(error) {
        console.log(error);
        return false;
    }
}