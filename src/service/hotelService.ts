import { Amenities, Hotel, Room, RoomType } from '../api/types/graphql-types';
import localCache from '../data/cache';
import { HotelService } from '../external/hotel-service/codegen';

const mapAmmenitiesToEnum: { [key: string]: Amenities } = {
    'air conditioning': Amenities.AirConditioning,
    bathtub: Amenities.Bathtub,
    'free_parking': Amenities.FreeParking,
    'hair_dryer': Amenities.HairDryer,
    heating: Amenities.Heating,
    iron: Amenities.Iron,
    kitchen: Amenities.Kitchen,
    parking: Amenities.Parking,
    wifi: Amenities.Wifi
};

const mapHotelToResponse = (hotelId: number, hotelInfo: any): Hotel => {
    const rooms = hotelInfo.rooms.map((room: { room_id: string; room_name: string; bed_count: number; amenities: Array<string>; room_type: string; }) => ({
        room_id: room.room_id,
        room_name: room.room_name,
        room_type: room.room_type.toUpperCase(),
        bedCount: room.bed_count,
        amenities: room.amenities.map(amenity => mapAmmenitiesToEnum[amenity])
    }));
    return {...hotelInfo, id: hotelId, rooms}
}

const getHotelFromApi = async (hotelId: number): Promise<Hotel> => {
    const key = `Hotel:${hotelId}`;
    const cachedHotel = localCache.get(key);
    if (cachedHotel) {
        return cachedHotel as Hotel;
    }
    const hotelInfo = await HotelService.getHotels(hotelId);
    const hotelObj = mapHotelToResponse(hotelId, hotelInfo);
    localCache.set(key, hotelObj);
    return hotelObj
};

const getHotelRoomsFromApi = async (hotelId: number, roomType?: RoomType | null): Promise<Array<Room>> => {
    const key = `Hotel:${hotelId}`;
    const cachedHotel = localCache.get(key);
    const filter = roomType ? (r: Room) => r.room_type === roomType : (r: Room) => true;
    if (cachedHotel) {
        return (cachedHotel as Hotel).rooms.filter(filter);
    }
    const hotelInfo = await HotelService.getHotels(hotelId);
    const hotelObj = mapHotelToResponse(hotelId, hotelInfo);
    localCache.set(key, hotelObj);
    return hotelObj.rooms.filter(filter);
};

export default {
    getHotel: async (hotelId: number): Promise<Hotel> => {
        try {
            return await getHotelFromApi(hotelId);
        } catch (error) {
            console.error(error);
            throw error;
        }
    },
    getHotelRooms: async ({ hotelId, roomType }: { hotelId: number, roomType: RoomType | null | undefined }): Promise<Array<Room>> => {
        try {
            return await getHotelRoomsFromApi(hotelId, roomType);
        } catch (error) {
            console.error(error);
            throw error;
        }
    }
};