import { sortBy } from 'lodash';
import { DateTime } from 'luxon';
import { MetricPrice, Metrics, Period, Price } from '../api/types/graphql-types';
import localCache from '../data/cache';
import { HotelService } from '../external/hotel-service/codegen';

const mapHotelRoomPrices = (hotelId: number, prices: any) => {
    console.log('Mapping room hotel prices');
    const mappedPrices = Object.keys(prices).map(roomId =>
    ({
        key: `${hotelId}:${roomId}`,
        val: prices[roomId][0]
    })
    );
    localCache.mset(mappedPrices);
    return mappedPrices;
};

const transformToMetrics = (priceOfDay: any) => {
    const mappedPrices: MetricPrice[] = Object.keys(priceOfDay)
        .filter(key => key !== 'date')
        .map((value) => {
            const { price, tax } = priceOfDay[value];
            return {
                gross_amount: price + tax,
                net_amount: price,
                competitor_name: value
            } as MetricPrice
        });
    const orderedPrices = sortBy(mappedPrices, ['gross_amount', 'asc']);

    const average = mappedPrices.reduce((prev, curr, _index, _arr) => {
        prev.gross_amount += curr.gross_amount;
        prev.net_amount += curr.net_amount;
        return prev;
    }, { gross_amount: 0, net_amount: 0, competitor_name: 'Average' } as MetricPrice);
    average.gross_amount = average.gross_amount / mappedPrices.length;
    average.net_amount = average.net_amount / mappedPrices.length;

    return {
        average_Price: average,
        best_price: orderedPrices[0],
        worst_price: orderedPrices[orderedPrices.length - 1]
    };
};

const mapPrices = (apiPrice: any): Array<Price> => {
    const prices: Array<Price> = [];
    Object.keys(apiPrice).forEach(key => {
        if (key !== 'date') {
            prices.push({
                date: apiPrice.date,
                competitor_name: key,
                currency: apiPrice[key].currency,
                taxes: apiPrice[key].tax,
                amount: apiPrice[key].price
            });
        }
    });
    return prices;
}

const formatDate = (date: DateTime): string => {
    return date.toFormat('dd/LL/yyyy');
};

const getNextThirtyDays = async (hotelId: number, startDate: DateTime): Promise<{ [key: string]: Array<Price> }> => {
    const endDate = startDate.plus({ days: 29 });
    
    const newPrices: { [key: string]: Array<Price>} = {};
    try {
        if (startDate.month === endDate.month) {
            const prices = await HotelService.getPrices(hotelId, formatDate(startDate), formatDate(endDate));
            Object.keys(prices.prices).forEach((roomId) => {
                const roomPrice = prices.prices[roomId][0];
                newPrices[roomId] = roomPrice.flatMap(mapPrices);
            });
        } else {
            const endOfMonth = startDate.endOf('month');
            const startOfNextMonth = endDate.startOf('month');
            const [prices, prices2] = await Promise.all([ 
                HotelService.getPrices(hotelId, formatDate(startDate), formatDate(endOfMonth)),
                HotelService.getPrices(hotelId, formatDate(startOfNextMonth), formatDate(endDate))
            ]);
            Object.keys(prices.prices).forEach((roomId) => {
                const roomPrice = prices.prices[roomId][0];
                newPrices[roomId] = roomPrice.flatMap(mapPrices);
            });
            Object.keys(prices2.prices).forEach((roomId) => {
                newPrices[roomId] = roomId in newPrices ? 
                    [...newPrices[roomId], ...prices2.prices[roomId][0].flatMap(mapPrices)]
                    : prices.prices2[roomId][0].flatMap(mapPrices) 
            });
        }

    } catch(error) {
        console.error(error);
    }
    return newPrices;
};

const periodToNumberOfDays = {
    [Period.Days_30]: 30,
    [Period.Days_60]: 60,
    [Period.Days_90]: 90
};

export default {
    getRoomMetrics: async ({ hotelId, roomId, day }: { hotelId: number, roomId: string, day: string }): Promise<Metrics> => {
        const cacheKey = `${hotelId}:${roomId}`;
        const cachedMetrics = localCache.get(cacheKey);
        let roomPrices;
        if (cachedMetrics) {
            console.log(`Getting metrics from cache for HotelId ${hotelId} RoomId: ${roomId}`);
            roomPrices = cachedMetrics as any;
        } else {
            console.log(`No cached values for HotelId ${hotelId} RoomId: ${roomId}`);
            const { prices } = await HotelService.getPrices(hotelId, day, day);
            roomPrices = mapHotelRoomPrices(hotelId, prices).find(price => price.key === cacheKey)?.val || [];
        }
        const priceOfDate = roomPrices.find((p: { date: string; }) => p.date === day);
        return transformToMetrics(priceOfDate);
    },
    getRoomPrices: async ({ hotelId, period, roomId, limit }: { hotelId: number, period: Period, roomId: string, limit: number }): Promise<Array<Price>> => {
        const roomsPrices: Array<Price> = [];
        for (let day = 0; day < periodToNumberOfDays[period]; day += 30) {
            const cacheKey = `${hotelId}:${roomId}:${day + 30}`;
            console.log(`Getting prices for DAYS_${day + 30} with key '${cacheKey}'`);
            const cachedPrices = localCache.get(cacheKey);
            if (cachedPrices) {
                console.log(`Getting from cache for HotelId: ${hotelId} RoomId: ${roomId} Period: ${day + 30}`);
                // @ts-ignore
                roomsPrices.push(...cachedPrices);
            } else {
                console.log('No cached value fetching from service');
                const prices = await getNextThirtyDays(hotelId, DateTime.now().plus({ days: day }));
                const newPrices = Object.keys(prices).map((roomId) =>
                ({
                    key: `${hotelId}:${roomId}:${day + 30}`,
                    val: prices[roomId]
                }));
                // console.log(`adding to cache ${newPrices.map(p => p.key)}`)
                localCache.mset(newPrices);
                roomsPrices.push(...prices[roomId]);
            }

        }
        if (limit)
            return roomsPrices.filter(p => p.amount + p.taxes <= limit!);
        else
            return roomsPrices
    }
};