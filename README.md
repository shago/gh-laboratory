
# Getting Started
## Creating the .env file
Create a .env file on root directory with the following values
```
HOTEL_SERVICE_URL=http://localhost:5000
MONGO_URL=mongodb://localhost:27017/gh-lab
TOKEN_SECRET=Secret_String_for_JWT
```
You can replace the values to fit you dev environment

## Build and run the project
* If you have a Mongo instance you can just run
`yarn` and `yarn start` to build the project and run it,

* If you prefer using docker to both the project and the mongoDb instance you can run `docker-compose up --build` 

After running the project you can go to [http://localhost:4000/graphql](http://localhost:4000/graphql)
and will see the following page
![Apollo Screen](./apollo_screen.png)
Just click the **"Query your server"** button this will take you to the Apollo Studio where you can see the Schema and query the server

Or you can import the Postman collection included in the project `Guru Hotel Lab.postman_collection.json`