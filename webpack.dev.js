const { merge } = require('webpack-merge');
const path = require('path');
const Dotenv = require('dotenv-webpack');
const NodemonPlugin = require('nodemon-webpack-plugin');
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  watch: true,
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    },
    devtool: 'inline-source-map',
    plugins: [
      new NodemonPlugin(),
      new Dotenv({
        path: path.resolve(__dirname, '.env'),
        systemvars: true,
        safe: true
      })
    ]
});
